package com.telerikacademy.finalproject.pages;

public class HomePage extends BasePage {

    public HomePage() {
        super("base.url");
    }


    public final String aboutUs = "homePage.AboutUs";
    public final String logoutButton = "navigation.Logout";


    public void assertPageLoaded() {
        actions.isElementPresentUntilTimeout(logoutButton, 15);
        assertPageNavigated();
    }
}