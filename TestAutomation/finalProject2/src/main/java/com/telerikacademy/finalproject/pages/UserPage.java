package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

import java.util.concurrent.TimeUnit;

public class UserPage extends BasePage {
    public UserPage() {
        super("base.url");
        driver.manage().timeouts().implicitlyWait(40, TimeUnit.SECONDS);
    }



    public void userEditOwnProfile() {


        actions.clickElement("user.profileButton");
        actions.clickElement("user.editUserProfileLink");
        actions.clearFieldText("user.firstNameField");
        actions.clickElement("user.firstNameField");
        String first = Utils.getUIMappingByKey("firstName");
        actions.typeValueInField(first, "user.firstNameField");
        actions.clearFieldText("user.lastNameField");
        actions.clickElement("user.lastNameField");
        String lastname = Utils.getUIMappingByKey("lastName");
        actions.typeValueInField(lastname, "user.lastNameField");
        actions.clickElement("user.birthdayField");
        String birthday = Utils.getUIMappingByKey("year");
        actions.typeValueInField(birthday, "user.birthdayField");
        actions.clickElement("user.gender");
        actions.clearFieldText("user.emailField");
        actions.clickElement("user.emailField");
        String email = Utils.getUIMappingByKey("mail");
        actions.typeValueInField(email, "user.emailField");
        actions.clearFieldText("user.yourselfField");
        actions.clickElement("user.yourselfField");
        String text = Utils.getUIMappingByKey("yourSelfText");
        actions.typeValueInField(text, "user.yourselfField");
        actions.clickElement("user.city");
        actions.clickElement("user.updateMyProfileButton");


    }

    public void changeProfessionalCategory() {

        actions.clickElement("user.profileButton");
        actions.clickElement("user.editUserProfileLink");
        actions.clickElement("user.professional");
        actions.clickElement("user.professionalUpdateButton");
    }


    public void updateService() {

        actions.clickElement("user.profileButton");
        actions.clickElement("user.editServices");
        String skillOne = Utils.getUIMappingByKey("skill1");
        actions.typeValueInField(skillOne, "user.updateYourService1");
        actions.clearFieldText("user.weeklyAvailability");
        String time = Utils.getUIMappingByKey("weekServicesTime");
        actions.typeValueInField(time, "user.weeklyAvailability");
        actions.clickElement("user.updateWeeklyAvailabilityButton");

    }

    public void sendFriendRequest() {
        actions.clickElement("user.searchUserField");
        String friend = Utils.getUIMappingByKey("userForSearch");
        actions.typeValueInField(friend, "user.searchUserField");
        actions.clickElement("user.searchButton");
        actions.clickElement("user.seeProfileButton");
        actions.clickElement("user.connectButton");
        actions.assertElementPresent("user.textFriendRequest");
    }


    public void acceptFriendRequest() {
        actions.isElementPresentUntilTimeout("user.personalProfileAssert", 10);
        actions.clickElement("user.profileButton");
        actions.clickElement("user.newFriendRequest");
        actions.clickElement("user.approveRequestButton");

    }

    public void disconnectFriendship() {

        actions.clickElement("user.searchUserField");
        String friend = Utils.getUIMappingByKey("userForSearch");
        actions.typeValueInField(friend, "user.searchUserField");
        actions.clickElement("user.searchButton");
        actions.clickElement("user.seeProfileButton");
        actions.clickElement("user.disconnectButton");


    }


    //***Asserts**//



    public void assertChangeProfessionalCategory() {
        actions.assertTextExists("Pilot");
    }

    public void assertSendFriendRequest() {
        actions.assertElementPresent("user.textFriendRequest");
    }

    public void assertNoRequestMessage() {
        actions.assertElementPresent("user.noRequestText");
    }

    public void assertDisconnectUser() {
        actions.assertElementPresent("user.connectButton");
    }

}

