package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;

import java.util.concurrent.TimeUnit;

public class PostPage extends BasePage {
    public PostPage() {
        super("base.url");



    }

    private final static String comment = "post.typeComment";


    //***Methods***///


    public void createPost() {
        actions.isElementPresentUntilTimeout("post.addNewPostButton", 10);
        actions.clickElement("post.addNewPostButton");
        actions.isElementPresentUntilTimeout("post.textCheck", 10);
        actions.isElementPresentUntilTimeout("post.dropDownMenu", 10);
        actions.clickElement("post.dropDownMenu");
        actions.clickElement("post.visibility");
        String postText = Utils.getUIMappingByKey("post.typePostText");
        actions.typeValueInField(postText, "post.postField");
        actions.clickElement("post.savePostButton");

    }


    public void editOwnPost() {

        actions.isElementPresentUntilTimeout("post.latestPostLink", 10);
        actions.clickElement("post.latestPostLink");
        actions.isElementPresentUntilTimeout("post.exploreButton", 10);
        actions.clickElement("post.exploreButton");
        actions.isElementPresentUntilTimeout("post.editPostButton", 10);
        actions.clickElement("post.editPostButton");
        actions.clickElement("post.visibility");
        actions.isElementPresentUntilTimeout("post.editPostField", 10);
        actions.clickElement("post.editPostField");
        String editUserPostText = Utils.getUIMappingByKey("post.editPostText");
        actions.typeValueInField(editUserPostText, "post.editPostField");
        actions.waitFor(20);
        actions.clickElement("post.editPostSaveButton");
    }


    public void createCommentOwnPost() {

        actions.isElementPresentUntilTimeout("post.latestPostLink", 10);
        actions.clickElement("post.latestPostLink");
        actions.isElementPresentUntilTimeout("post.assertPost", 20);
        actions.isElementPresentUntilTimeout("post.exploreButton", 10);
        actions.waitFor(1000);
        actions.clickElement("post.exploreButton");
        actions.assertElementPresent("post.assertComment");
        actions.isElementPresentUntilTimeout("post.commentField", 10);
        actions.clickElement("post.commentField");
        String commentText = Utils.getUIMappingByKey("post.typeComment");
        actions.typeValueInField(commentText, "post.commentField");
        actions.clickElement("post.commentButton");


    }

    public void userEditOwnComment() {
        actions.isElementPresentUntilTimeout("post.latestPostLink", 10);
        actions.clickElement("post.latestPostLink");
        actions.isElementPresentUntilTimeout("post.assertPost", 20);
        actions.isElementPresentUntilTimeout("post.exploreButton", 20);
        actions.clickElement("post.exploreButton");
        actions.isElementPresentUntilTimeout("post.showCommentButton", 10);
        actions.clickElement("post.showCommentButton");
        actions.waitFor(500);
        actions.assertElementPresent("post.editComment");
        actions.waitFor(30);
        actions.clickElement("post.editComment");
        actions.assertElementPresent("post.editCommentAssert");
        actions.isElementPresentUntilTimeout("post.editCommentField", 10);
        actions.clickElement("post.editCommentField");
        String editCommentText = Utils.getUIMappingByKey("post.typeEditComment");
        actions.typeValueInField(editCommentText, "post.editCommentField");
        actions.isElementPresentUntilTimeout("post.editCommentButton", 10);
        actions.waitFor(50);
        actions.clickElement("post.editCommentButton");

    }

    public void userDeleteOwnPost() {


        actions.isElementPresentUntilTimeout("post.latestPostLink", 10);
        actions.clickElement("post.latestPostLink");
        actions.assertElementPresent("post.assertPost");
        actions.isElementPresentUntilTimeout("post.exploreButton", 10);
        actions.clickElement("post.exploreButton");
        actions.isElementPresentUntilTimeout("post.deletePostButton", 10);
        actions.clickElement("post.deletePostButton");
        actions.assertElementPresent("post.assertDeleteText");
        actions.waitFor(3000);
        actions.clickElement("post.assertDelete");
        actions.isElementPresentUntilTimeout("post.deletePostSubmitButton", 10);
        actions.clickElement("post.deletePostSubmitButton");

    }


    public void userDeleteOwnComment() {
        actions.isElementPresentUntilTimeout("post.latestPostLink", 10);
        actions.clickElement("post.latestPostLink");
        actions.isElementPresentUntilTimeout("post.assertPost", 10);
        actions.isElementPresentUntilTimeout("post.exploreButton", 10);
        actions.clickElement("post.exploreButton");
        actions.isElementPresentUntilTimeout("post.showCommentButton", 10);
        actions.clickElement("post.showCommentButton");
        actions.waitFor(500);
        actions.assertElementPresent("post.deleteCommentButton");
        actions.waitFor(300);
        actions.clickElement("post.deleteCommentButton");
        actions.isElementPresentUntilTimeout("post.deleteCommentAssertTextPage", 10);
        actions.waitFor(200);
        actions.assertElementPresent("post.clickDdl");
        actions.clickElement("post.clickDdl");
        actions.clickElement("post.ddlForDelete");
        actions.waitFor(100);
        actions.isElementPresentUntilTimeout("post.confirmationDeleteComment ", 10);
        actions.clickElement("post.confirmationDeleteComment");


    }

    public void likeUserPost() {
        actions.isElementPresentUntilTimeout("post.latestPostLink", 10);
        actions.clickElement("post.latestPostLink");
        actions.clickElement("post.likeButton");


    }


    public void dislikeUserPost() {
        actions.isElementPresentUntilTimeout("post.latestPostLink", 10);
        actions.clickElement("post.latestPostLink");
        actions.clickElement("post.dislikeButton");

    }


    //*****Asserts**//

    public void assertPostIsCreated() {
        actions.assertElementPresent("post.assertPost");

    }

    public void assertPostIsDelete() {
        actions.isElementPresentUntilTimeout("post.confirmationDeleteText", 10);
    }

    public void assertPostIsCommented() {
        actions.waitFor(2000);
        actions.isElementPresentUntilTimeout("post.showCommentButton", 10);
        actions.waitFor(2000);
        actions.clickElement("post.showCommentButton");
        actions.assertTextExists(comment);


    }
    public void assertPostIsLiked(){
        actions.assertElementPresent("post.dislikeButton");
    }

    public void assertPostIsDisliked (){
        actions.assertElementPresent("post.likeButton");
    }

    public void assertDeleteComment (){
        actions.assertElementPresent("post.assertDeleteComment");
    }


}
