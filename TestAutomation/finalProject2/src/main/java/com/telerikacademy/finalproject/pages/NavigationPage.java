package com.telerikacademy.finalproject.pages;

public class NavigationPage extends BasePage{


    public NavigationPage() {
        super("base.url"); }



    public final String homeButton = "navigation.Home";
    public final String signInButton = "login.singIn";
    public final String signUpButton = "navigation.SignUp";
    public final String logOutButton = "navigation.logOutButton";
    public final String latestPosts = "navigation.LatestPosts";
    public final String registerButton = "navigation.Register";



}


