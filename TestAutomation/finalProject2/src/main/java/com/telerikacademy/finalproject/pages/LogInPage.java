package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;
import org.junit.internal.runners.model.EachTestNotifier;


public class LogInPage extends BasePage {

    public LogInPage() {
        super("login.url");

        //driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }



    //*************Methods*********//

    String usernameAd = Utils.getConfigPropertyByKey("usernameAdmin");
    String passwordAd = Utils.getConfigPropertyByKey("passwordAdmin");

    public void loginUser(String usernameUser, String passwordUser) {
        String username = Utils.getConfigPropertyByKey(usernameUser);
        String password = Utils.getConfigPropertyByKey(passwordUser);
        actions.isElementPresentUntilTimeout("login.singIn", 10);
        actions.clickElement("login.singIn");
        actions.isElementPresentUntilTimeout("login.userNameField", 10);
        actions.typeValueInField(username, "login.usernameField");
        actions.typeValueInField(password, "login.passwordField");
        actions.clickElement("login.button");
        actions.waitFor(10);
    }


    public void loginUserAdmin() {
        actions.isElementPresentUntilTimeout("login.singIn", 20);
        actions.clickElement("login.singIn");
        actions.isElementPresentUntilTimeout("login.userNameField", 20);
        actions.typeValueInField(usernameAd, "login.usernameField");
        actions.typeValueInField(passwordAd, "login.passwordField");
        actions.clickElement("login.button");
        actions.waitFor(20);

    }

    public void verifyLoginPageElements (){
        actions.assertElementPresent("login.singIn");
        actions.assertElementPresent("login.homeButton");
        actions.assertElementPresent("login.latestPostsButton");
        actions.assertElementPresent("login.usernameField");
        actions.assertElementPresent("login.passwordField");
        actions.assertElementPresent("login.button");
    }




    public void logoutUser() {
        actions.isElementPresentUntilTimeout("login.logoutButton", 25);
        actions.clickElement("login.logoutButton");

    }


    //***Asserts****//

    public void assertLogOut() {
        actions.assertTextExists("You have been logged out.");

    }

    public void assertUserCannotLogin() {
        actions.assertElementPresent("login.wrong");
    }

    public void assertUserLogin() {
        actions.assertElementPresent("login.logoutButton");
    }


}
