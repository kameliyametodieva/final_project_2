package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;


public class RegisterPage extends BasePage {
    public RegisterPage() {
        super("register.url");
    }




    public void verifyRegisterFields_ArePresent() {
        actions.assertElementPresent("login.singIn");
        actions.assertElementPresent("login.homeButton");
        actions.assertElementPresent("login.latestPostsButton");
        actions.assertElementPresent("registrationPage.username");
        actions.assertElementPresent("registrationPage.email");
        actions.assertElementPresent("registrationPage.confirmPassword");
        actions.assertElementPresent("registrationPage.dropDownCategory");
        actions.assertElementPresent("registrationPage.button");
   }




    public void registerUser_WithMandatoryFields (String username, String email ,String password, String confirmPassword
                                                  ) {
        String usernameReg = Utils.getConfigPropertyByKey(username);
        String emailReg = Utils.getConfigPropertyByKey(email);
        String passwordReg = Utils.getConfigPropertyByKey(password);
        String confirmPasswordReg = Utils.getConfigPropertyByKey(confirmPassword);
        actions.typeValueInField(usernameReg, "registrationPage.username");
        actions.typeValueInField(emailReg, "registrationPage.email");
        actions.typeValueInField(passwordReg, "registrationPage.password");
        actions.typeValueInField(confirmPasswordReg, "registrationPage.confirmPassword");
        actions.isElementPresentUntilTimeout("registrationPage.button", 10);
        actions.clickElement("registrationPage.button");



   }

    //***Asserts**//

    public void assertRegistration(){
        actions.assertElementPresent("registrationPage.confirmMessage");


    }

    public void assertUnsuccessfulRegistrationWithExistingUsername (){
        actions.assertElementPresent("registrationPage.existingUsernameMessage");
    }

    public void assertUnsuccessfulRegistrationWithDifferentPasswords (){
        actions.assertElementPresent("registrationPage.notConfirmedPasswordMessage");
    }

    public void assertUnsuccessfulRegistrationWithWrongEmailFormat (){
        actions.assertElementPresent("registrationPage.wrongEmailFormat");
    }





    }


