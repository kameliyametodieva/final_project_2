package com.telerikacademy.finalproject.pages;

import com.telerikacademy.finalproject.utils.Utils;
import org.junit.Assert;

import java.util.concurrent.TimeUnit;

public class AdminPage extends BasePage {
    public AdminPage() {
        super("base.url");
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

    }


    //****Methods***//


    //***User Functionality**//


    public void verifyAdminCanSeeAllUsers() {


        actions.clickElement("admin.goToAdminZoneLink");
        actions.clickElement("admin.viewUsers");


    }


    public void adminDisableUserProfile() {


        actions.clickElement("user.searchUserField");
        String name = Utils.getConfigPropertyByKey("usernameUser4");
        actions.typeValueInField(name, "user.searchUserField");
        actions.clickElement("user.searchUserField");
        actions.assertElementPresent("user.searchButton");
        actions.clickElement("user.searchButton");
        actions.clickElement("admin.seeProfile");
        actions.clickElement("admin.disable");
    }


    public void adminEnableUserProfile() {


        actions.clickElement("user.searchUserField");
        String name = Utils.getConfigPropertyByKey("usernameUser4");
        actions.typeValueInField(name, "user.searchUserField");
        actions.clickElement("user.searchUserField");
        actions.assertElementPresent("user.searchButton");
        actions.clickElement("user.searchButton");
        actions.clickElement("admin.seeProfile");
        actions.clickElement("admin.enable");


    }

    //******Post Functionality****//

    public void adminEditPublicUserPost() {

        actions.isElementPresentUntilTimeout("post.latestPostLink", 10);
        actions.clickElement("post.latestPostLink");
        actions.clickElement("post.allPublicPostButton");
        actions.isElementPresentUntilTimeout("post.exploreButton", 10);
        actions.clickElement("post.exploreButton");
        actions.isElementPresentUntilTimeout("post.editPostButton", 10);
        actions.clickElement("post.editPostButton");
        actions.clickElement("post.visibility");
        actions.isElementPresentUntilTimeout("post.editPostField", 10);
        actions.clickElement("post.editPostField");
        String editUserPostText = Utils.getUIMappingByKey("post.editPostTextAdmin");
        actions.typeValueInField(editUserPostText, "post.editPostField");
        actions.waitFor(20);
        actions.clickElement("post.editPostSaveButton");
    }

    public void adminDeletePublicPost() {


        actions.isElementPresentUntilTimeout("post.latestPostLink", 10);
        actions.clickElement("post.latestPostLink");
        actions.clickElement("post.allPublicPostButton");
        actions.assertElementPresent("post.adminAssertDelete");
        actions.isElementPresentUntilTimeout("post.exploreButton", 10);
        actions.clickElement("post.exploreButton");
        actions.isElementPresentUntilTimeout("post.deletePostButton", 10);
        actions.clickElement("post.deletePostButton");
        actions.assertElementPresent("post.assertDeleteText");
        actions.waitFor(3000);
        actions.clickElement("post.assertDelete");
        actions.isElementPresentUntilTimeout("post.deletePostSubmitButton", 10);
        actions.clickElement("post.deletePostSubmitButton");

    }


    //*******Assert***//

    public void assertUserDisable() {
        actions.assertElementPresent("admin.enable");
    }

    public void assertUserEnable() {
        actions.assertElementPresent("admin.disable");
    }


    public void assertUserListPageNavigated() {
        String currentUrl = driver.getCurrentUrl();
        String urlUserList = Utils.getConfigPropertyByKey("admin.urlUserList");
        Assert.assertTrue("Landed URL is not as expected. Actual URL: " + currentUrl +
                ". Expected URL: " + urlUserList, currentUrl.contains(urlUserList));
    }

    public void assertPublicPostIsUpdated() {
        actions.assertElementPresent("post.assertAdminEditPostText");

    }

    public void assertPostIsDelete() {
        actions.isElementPresentUntilTimeout("post.confirmationDeleteText", 10);
    }

    public void assertDeleteCommentByAdmin() {
        actions.assertElementPresent("post.assertDeleteComment");
    }
}












