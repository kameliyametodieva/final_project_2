package com.telerikacademy.finalproject.utils;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class UserActions {

    public WebDriver getDriver() {
        return driver;
    }

    final WebDriver driver;

    public UserActions() {
        this.driver = Utils.getWebDriver();
    }

    public static void loadBrowser(String baseUrlKey) {
        Utils.getWebDriver().get(Utils.getConfigPropertyByKey(baseUrlKey));
    }

    public static void quitDriver() {
        Utils.tearDownWebDriver();
    }

    //############# ELEMENT OPERATIONS #########

    public void clickElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);

        Utils.LOG.info("Clicking on element " + key);
        WebElement element = driver.findElement(By.xpath(locator));
        element.click();
    }

    public void hoverOnElement(String key, Object... arguments) {
        String locator = Utils.getUIMappingByKey(key, arguments);
        Actions action = new Actions(driver);
        WebElement element = driver.findElement(By.xpath(locator));
        action.moveToElement(element).build().perform();
    }

    public void clickElementInList(String key, String elementName, Object... arguments) {
//        String locator = Utils.getUIMappingByKey(key, arguments);
//        Utils.LOG.info("Clicking on element " + key);
//        List<WebElement> elements = driver.findElements(By.xpath(locator));
//        for (int i = 0; i < elements.size(); i++) {
//            if (elements.get(i).getText().equalsIgnoreCase(elementName)) {
//                elements.get(i).click();
//                break;
//            }
//        }
        String locator = Utils.getUIMappingByKey(key, arguments);
        Select objSelect = new Select(driver.findElement(By.xpath(locator)));
        objSelect.selectByVisibleText(elementName);
    }

    public void typeValueInField(String value, String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.sendKeys(value);
    }

    public void switchToIFrame(String iframe) {
        WebElement iFrame = driver.findElement(By.xpath(Utils.getUIMappingByKey(iframe)));
        getDriver().switchTo().frame(iFrame);
    }


    public void clearFieldText(String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.clear();


    }

    public void clearField(String field, Object... fieldArguments) {
        String locator = Utils.getUIMappingByKey(field, fieldArguments);
        WebElement element = driver.findElement(By.xpath(locator));
        element.clear();
    }


    public void uploadPicture(String value, String keys) {
        String inputFieldPicture = Utils.getUIMappingByKey("value");
        String key = Utils.getConfigPropertyByKey("keys");


        driver.findElement(By.xpath("//input[@id='file']")).sendKeys("C:\\Users\\kami\\Desktop\\download.jpg");
    }

    public void waitFor(long milliseconds) {
        try {
            Thread.sleep(milliseconds);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


    //############# WAITS #########


    public void clickElementAfterWait(String locator) {
        isElementPresentUntilTimeout(locator, 3);
        clickElement(locator);
    }


    public boolean isElementPresentUntilTimeout(String locator, int timeout, Object... arguments) {
        try {
            String xpath = Utils.getUIMappingByKey(locator, arguments);
            Integer defaultTimeout = Integer.parseInt(Utils.getConfigPropertyByKey("config.defaultTimeoutSeconds"));
            WebDriverWait wait = new WebDriverWait(driver, timeout);
            wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath(xpath)));
            return true;
        } catch (Exception exception) {
            return false;
        }


    }

    public void waitForElementVisible(String locator, int seconds) {
        WebElement element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator)));
        WebDriverWait wait = new WebDriverWait(driver, seconds);
        try {
            wait.until(ExpectedConditions.visibilityOf(element));
        } catch (Exception exception) {
            Assert.fail("Element with locator:'" + locator + "'was not found");
        }
    }

    public static void waitUntilIsClickable(WebDriver driver, String locator) {
        WebDriverWait wait = new WebDriverWait(driver, 8, 50);
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath(locator)));
    }


    //############# ASSERTS #########

    public void assertElementPresent(String locator) {
        Assert.assertNotNull(driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))));
    }

    public void assertElementIsNotPresent(String locator, String containsText) {
        String text = Utils.getUIMappingByKey(containsText);
        String element = driver.findElement(By.xpath(Utils.getUIMappingByKey(locator))).getText();
        Assert.assertFalse(element.contains(text));
    }

    public void assertTextExists(String text) {
        Assert.assertTrue("You have been logged out.", driver.getPageSource().contains(text));
    }

}



