package testCases.adminFunctionality;

import com.telerikacademy.finalproject.pages.AdminPage;
import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import org.junit.*;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;

import javax.xml.bind.annotation.XmlElementWrapper;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class AdminDisableAndEnablesUserTests extends BaseTest {

    AdminPage adminPage = new AdminPage();


    @Before
    public void beforeTest() {
        adminPage.navigateToPage();
        adminPage.assertPageNavigated();
    }


    @Test
    public void a_loginUserAdmin() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.loginUserAdmin();
        loginPage.assertUserLogin();
    }




    @Test
    public void b_verifyAdminCanSeeAllUsers() {
        adminPage.verifyAdminCanSeeAllUsers();
        adminPage.assertUserListPageNavigated();

    }


    @Test
    public void c_adminDisableUserProfile() {
        adminPage.adminDisableUserProfile();
        adminPage.assertUserDisable();

    }

    @Test
    public void d_logoutUserAdmin() {
        LogInPage loginPage = new LogInPage();
        loginPage.logoutUser();
        loginPage.assertLogOut();

    }

    @Test
    public void e_disabledUserCannotLoginIn() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.assertPageNavigated();
        loginPage.loginUser("usernameUser4", "passwordUser4");
        loginPage.assertUserCannotLogin();
    }

    @Test
    public void f_loginUserAdmin() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.loginUserAdmin();
    }


    @Test
    public void g_adminEnableUserProfile() {
        adminPage.adminEnableUserProfile();
        adminPage.assertUserEnable();


    }

    @Test
    public void h_logoutAdmin() {
        LogInPage loginPage = new LogInPage();
        loginPage.logoutUser();
        loginPage.assertLogOut();

    }

    @Test
    public void i_enabledUserCanLoginIn() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.assertPageNavigated();
        loginPage.loginUser("usernameUser4", "passwordUser4");
        loginPage.assertUserLogin();
    }

    @Test
    public void j_logoutUser() {
        LogInPage loginPage = new LogInPage();
        loginPage.logoutUser();
        loginPage.assertLogOut();

    }


}





