package testCases.adminFunctionality;

import com.telerikacademy.finalproject.pages.AdminPage;
import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import testCases.BaseTest;

public class AdminPublicPostFunctionalityTests extends BaseTest {

    AdminPage adminPage = new AdminPage();


    @Before
    public void beforeTest() {
        adminPage.navigateToPage();
        adminPage.assertPageNavigated();
    }


    @BeforeClass
    public static void loginUserAdmin() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.loginUserAdmin();
        loginPage.assertUserLogin();
    }

    @Test

    public void a_adminEditPublicUserPost() {
        adminPage.adminEditPublicUserPost();
        adminPage.assertPublicPostIsUpdated();
    }


    @Test
    public void c_adminDeletePublicPost() {
        adminPage.adminDeletePublicPost();
        adminPage.assertPostIsDelete();
    }

    @Test
    public void d_logoutUser() {
        LogInPage loginPage = new LogInPage();
        loginPage.logoutUser();
        loginPage.assertLogOut();

    }


}
