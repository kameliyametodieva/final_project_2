package testCases.connectionFunctionality;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.UserPage;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class ConnectionsTests extends BaseTest {

    UserPage userPage = new UserPage();


    @Test
    public void a_loginUser2() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.assertPageNavigated();
        loginPage.loginUser("usernameUser2", "passwordUser2");
        loginPage.assertUserLogin();


    }

    @Test
    public void b_sendFriendRequest() {
        userPage.sendFriendRequest();
        userPage.assertSendFriendRequest();

    }

    @Test
    public void c_logoutUser2() {
        LogInPage loginPage = new LogInPage();
        loginPage.logoutUser();
        loginPage.assertLogOut();
    }

    @Test
    public void d_loginUser3() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.assertPageNavigated();
        loginPage.loginUser("usernameUser3", "passwordUser3");
        loginPage.assertUserLogin();
    }



    @Test
    public void e_acceptFriendRequest() {
        userPage.acceptFriendRequest();
        //userPage.assertNoRequestMessage();

    }

    @Test
    public void f_logoutUser3() {
        LogInPage loginPage = new LogInPage();
        loginPage.logoutUser();
        loginPage.assertLogOut();
    }

    @Test
    public void g_loginUser2() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.assertPageNavigated();
        loginPage.loginUser("usernameUser2", "passwordUser2");
        loginPage.assertUserLogin();
    }

    @Test
    public void h_disconnectFriendship (){
        userPage.disconnectFriendship();
        userPage.assertDisconnectUser();

    }

    @Test
    public void i_logoutUser1() {
        LogInPage loginPage = new LogInPage();
        loginPage.logoutUser();
        loginPage.assertLogOut();


}}


