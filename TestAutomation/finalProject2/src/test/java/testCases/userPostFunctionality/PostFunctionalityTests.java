package testCases.userPostFunctionality;


import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.PostPage;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class PostFunctionalityTests extends BaseTest {

    PostPage postPage = new PostPage();

    @BeforeClass
    public static void loginUser() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.assertPageNavigated();
        loginPage.loginUser("usernameUser1", "passwordUser1");

    }

    @Before
    public void beforeTest() {
        postPage.navigateToPage();
        postPage.assertPageNavigated();


    }

    @Test
    public void a_createPost() {
        postPage.createPost();
        postPage.assertPostIsCreated();

    }

    @Test
    public void b_likeUserPost() {
        postPage.likeUserPost();
        postPage.assertPostIsLiked();
    }

    @Test
    public void c_dislikePost() {
        postPage.dislikeUserPost();
        postPage.assertPostIsDisliked();
    }

    @Test
    public void d_createComment() {
        postPage.createCommentOwnPost();

    }

    @Test
    public void e_userEditOwnComment() {
        postPage.userEditOwnComment();

    }

    @Test
    public void f_userDeleteOwnComment() {
        postPage.userDeleteOwnComment();
        postPage.assertDeleteComment();

    }

    @Test
    public void g_editOwnPost() {
        postPage.editOwnPost();

    }


    @Test
    public void h_userOwnDeletePost() {
        postPage.userDeleteOwnPost();
        postPage.assertPostIsDelete();
    }

    @Test
    public void j_logout() {
        LogInPage loginPage = new LogInPage();
        loginPage.logoutUser();
        loginPage.assertLogOut();
    }


}



