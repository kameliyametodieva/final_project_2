package testCases.userProfileFunctionality;

import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.UserPage;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import testCases.BaseTest;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)

public class EditUserProfileTests extends BaseTest {

    UserPage userPage = new UserPage();


    @BeforeClass
    public static void loginUser() {
        NavigationPage navPage = new NavigationPage();
        navPage.actions.clickElement(navPage.signInButton);
        LogInPage loginPage = new LogInPage();
        loginPage.loginUser("usernameUser1", "passwordUser1");
    }


    @Test
    public void a_userEditOwnProfile() {
        userPage.userEditOwnProfile();
        //userPage.assertProfileUpdate();

    }

    @Test
    public void b_changeProfessionalCategory() {
        userPage.changeProfessionalCategory();
        userPage.assertChangeProfessionalCategory();

    }

    @Test
    public void c_updateServices() {
        userPage.updateService();

    }
    @Test
    public void d_logout(){
        LogInPage loginPage = new LogInPage();
        loginPage.logoutUser();
        loginPage.assertLogOut();
    }
}





