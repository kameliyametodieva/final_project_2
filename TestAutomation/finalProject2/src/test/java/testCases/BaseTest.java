package testCases;

import com.telerikacademy.finalproject.utils.UserActions;
import com.telerikacademy.finalproject.utils.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;

public class BaseTest {
	public UserActions actions = new UserActions();

	@BeforeClass
	public static void setUp(){
		UserActions.loadBrowser("base.url");
	}

 	@AfterClass
 	public static void tearDown(){
		UserActions.quitDriver();
	}
}
