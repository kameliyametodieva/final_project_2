package testCases;

import com.telerikacademy.finalproject.pages.HomePage;
import com.telerikacademy.finalproject.pages.LogInPage;
import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.utils.Utils;
import org.junit.*;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class LoginTests extends BaseTest {

    public final String username = "usernameUser1";
    public final String password = "passwordUser1";
    public final String wrongUsername = "wrongUsername";
    public final String wrongPassword = "wrongPassword";
    public final String emptyUsername = "";
    public final String emptyPassword = "";


    LogInPage loginPage = new LogInPage();


    @Before
    public void beforeTest() {

        loginPage.navigateToPage();
        loginPage.assertPageNavigated();


    }


    @Test
    public void a_verifyLoginPageElements() {
        loginPage.verifyLoginPageElements();



    }

    @Test
    public void b_userCanNotLogInWithEmptyUsernameAndCorrectPassword() {
        loginPage.loginUser(emptyUsername, password);
        loginPage.assertUserCannotLogin();


    }


    @Test
    public void c_userCanNotLoginWithCorrectUsernameAndEmptyPassword() {

        loginPage.loginUser(username, emptyPassword);
        loginPage.assertUserCannotLogin();
    }


    @Test
    public void d_userCanNotLoginWithCorrectUsernameAndWrongPassword() {

        loginPage.loginUser(username, wrongPassword);
        loginPage.assertUserCannotLogin();

    }

    @Test
    public void e_userCanNotLoginWithWrongUsernameAndCorrectPassword() {


        loginPage.loginUser(wrongUsername, password);
        loginPage.assertUserCannotLogin();
    }

    @Test
    public void f_userCannotLoginWithEmptyUsernameAndEmptyPasswordFields (){
        loginPage.loginUser(emptyUsername, emptyPassword);
        loginPage.assertUserCannotLogin();
    }


    @Test
    public void g_userSuccessfullyLoggingIn() {
        loginPage.loginUser(username, password);
        loginPage.assertUserLogin();
    }


    @Test
    public void h_userSuccessfullyLogout() {
        loginPage.logoutUser();
        loginPage.assertLogOut();

    }
}


