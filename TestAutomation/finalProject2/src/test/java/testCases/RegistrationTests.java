package testCases;


import com.telerikacademy.finalproject.pages.NavigationPage;
import com.telerikacademy.finalproject.pages.RegisterPage;
import com.telerikacademy.finalproject.utils.Utils;
import org.jsoup.Connection;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegistrationTests extends BaseTest {


    public final String username = "username";
    public final String email = "email";
    public final String wrongEmail= "wrongEmail";
    public final String password = "password";
    public final String confirmationPassword = "confirmPassword";
    public final String wrongPassword = "wrongPassword";
    public final String emptyUsername = "";
    public final String emptyPassword = "";




    public RegisterPage registerPage;

    @Before
    public void beforeTest() {
        registerPage = new RegisterPage();
        registerPage.navigateToPage();
        registerPage.assertPageNavigated();
    }

    @Test
    public void a_verifyRegisterFields_ArePresent() {
        registerPage.verifyRegisterFields_ArePresent();

    }

    @Test
    public void b_userRegistration () {
        registerPage.registerUser_WithMandatoryFields(username,email, password, confirmationPassword);
        registerPage.assertRegistration ();


    }
    @Test
    public void c_unsuccessfullyRegistrationWithExistingUsername (){
        registerPage.registerUser_WithMandatoryFields(username,email, password, confirmationPassword);
        registerPage.assertUnsuccessfulRegistrationWithExistingUsername();


    }
    @Test
    public void d_unsuccessfullyRegistrationWithDifferentPasswords (){
        registerPage.registerUser_WithMandatoryFields(username,email, password, wrongPassword);
        registerPage.assertUnsuccessfulRegistrationWithDifferentPasswords();
    }

    @Test
    public void e_unsuccessfullyRegistrationWithEmptyUsernameField (){
        registerPage.registerUser_WithMandatoryFields(emptyUsername,email, password, wrongPassword);
    }

    @Test
    public void f_unsuccessfullyRegistrationWithEmptyFieldPassword (){
        registerPage.registerUser_WithMandatoryFields(username,email,emptyPassword,password);
        registerPage.assertUnsuccessfulRegistrationWithDifferentPasswords();


    }
    @Test
    public void g_unsuccessfullyRegistrationWithEmptyFieldConfirmationPassword (){
        registerPage.registerUser_WithMandatoryFields(username,email,password,emptyPassword);
        registerPage.assertUnsuccessfulRegistrationWithDifferentPasswords();
    }
    @Test
    public void h_unsuccessfullyRegistrationWithIncorrectEmailFormat (){
        registerPage.registerUser_WithMandatoryFields(username,wrongEmail,password,confirmationPassword);
        registerPage.assertUnsuccessfulRegistrationWithWrongEmailFormat();
    }










}
